import 'package:flutter/material.dart';

class ExpandedExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Expanded Example"),
      ),
      body: Center(
        child: Column(
          children: [
            Spacer(
              flex: 2,
            ),
            Center(
              child: Text(
                "Without Expanded",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),
            ),
            Center(
              child: Row(
                children: [
                  Container(
                    height: screen.height / 10,
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Center(
                      child: Text(
                        "Flex 1",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                    color: Colors.amber,
                  ),
                  Container(
                    height: screen.height / 10,
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Center(
                      child: Text(
                        "Flex 2",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                    color: Colors.black,
                  ),
                  Container(
                    height: screen.height / 10,
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Center(
                      child: Text(
                        "Flex 1",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                    color: Colors.red,
                  )
                ],
              ),
            ),
            Spacer(
              flex: 2,
            ),
            Center(
              child: Text(
                "With Expanded",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),
            ),
            Center(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      height: screen.height / 10,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Center(
                        child: Text(
                          "Flex 1",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                      color: Colors.amber,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: screen.height / 10,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Center(
                        child: Text(
                          "Flex 2",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                      color: Colors.black,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: screen.height / 10,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Center(
                        child: Text(
                          "Flex 1",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                      color: Colors.red,
                    ),
                  )
                ],
              ),
            ),
            Spacer(
              flex: 2,
            ),
          ],
        ),
      ),
    );
  }
}
