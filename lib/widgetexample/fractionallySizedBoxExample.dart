import 'package:flutter/material.dart';

class FractionallySizedBoxExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Replace with the size your parent widget is.
    final pWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('FractionallySizedBox'),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top:50),
        child: Row(
          children: <Widget>[
            Container(
              height: 200,
              width: pWidth * 0.25,
              color: Colors.orange,
            ),
            Container(
              height: 200,
              width: pWidth * 0.15,
              color: Colors.green,
            ),
            Container(
              height: 200,
              width: pWidth * 0.05,
              color: Colors.blue,
            ),
          ],
          mainAxisAlignment:
              MainAxisAlignment.center, // Depending on what you want
        ),
      ),
    );
  }
}
