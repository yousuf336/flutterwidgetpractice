import 'package:flutter/material.dart';

class SafeAreaExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SafeArea Example"),
      ),
      body:Center(
        child: SafeArea(
            left: false,
            top: false,
            right: false,
            bottom: false,
            child: Text('Safe Area: ...'),
          ),
      )
      );
  }
}
