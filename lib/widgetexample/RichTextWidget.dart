import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class RichTextExample extends StatefulWidget {
  RichTextExample({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RichTextExampleState createState() => new _RichTextExampleState();
}

class _RichTextExampleState extends State<RichTextExample> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new DefaultTextStyle(
                style: Theme.of(context).textTheme.title,
                child: new RichWidget()),
            new Text(
              'You have pushed the button this many times:',
            ),
            new Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ),
    );
  }
}

class RichWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new RichText(
      text: new TextSpan(
        text: 'Hello ',
        style: DefaultTextStyle.of(context).style,
        children: <TextSpan>[
          new TextSpan(
              text: 'bold', style: new TextStyle(fontWeight: FontWeight.bold)),
          new TextSpan(
              text: ' world!',
              style: new TextStyle(fontWeight: FontWeight.normal)),
          new TextSpan(
              text: ' click Here!',
              style: new TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.blue),
              recognizer: new TapGestureRecognizer()..onTap = () {
                final snackBar = SnackBar(content: Text('Yay! Clicked!'));

                 // Find the Scaffold in the widget tree and use it to show a SnackBar.
                Scaffold.of(context).showSnackBar(snackBar);
              }),
        ],
      ),
    );
  }
}
