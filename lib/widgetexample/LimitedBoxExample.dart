import 'package:flutter/material.dart';


class LimitedBoxWidget extends StatelessWidget {
  // LimitedBoxWidget({
  //   Key key,
  //   double maxWidth: double.infinity,
  //   double maxHeight: double.infinity,
  //   Widget child,
  // });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('LimitedBoxExample'),
      ),
      body: Center(
        child: ListView.builder(
          itemCount: 5,
          itemBuilder: (context, index) {
            return LimitedBox(
              maxHeight: 70,
              child: CardList(index),
            );
          },
        ),
      ),
    );
  }
}
class CardList extends StatelessWidget {
  int index;
  CardList(
      this.index, {
        Key key,
      }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(
          Icons.album,
          size: 70,
        ),
        title: Text(
          'Item $index',
        ),
      ),
    );
  }
}
