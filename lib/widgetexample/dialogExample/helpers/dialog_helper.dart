
import 'package:flutter/material.dart';
import 'package:flutter_widget_example/widgetexample/dialogExample/dialog/ConfirmationDialog.dart';

class DialogHelper {

  static exit(context) => showDialog(context: context, builder: (context) => ConfirmationDialog());
}