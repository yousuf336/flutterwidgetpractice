import 'package:flutter/material.dart';

class DividerExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text('Divider Example'),
      ),
      body:  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center ,
          children: <Widget>[
            Container(
              height: 100,
              color: Colors.red,
            ),
            Divider(
              thickness: 10,
              indent: 10,
              color: Colors.green,
              height: 100,
              endIndent: 10,
            ),
            Container(
              height: 100,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}
