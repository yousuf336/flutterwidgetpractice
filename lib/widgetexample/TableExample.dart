import 'package:flutter/material.dart';

class TableExample extends StatefulWidget {
  @override
  _TableExampleState createState() => _TableExampleState();
}

class _TableExampleState extends State<TableExample> {
  double iconSize = 40;
  static const int numItems = 10;
  List<bool> selected = List<bool>.generate(numItems, (index) => false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Table Example'),
        ),
        body: ListView(
          children: [
            Column(children: <Widget>[
              Center(
                  child: Column(children: <Widget>[
                Container(
                  margin: EdgeInsets.all(10),
                  child: Table(
                    defaultColumnWidth:
                        FixedColumnWidth(MediaQuery.of(context).size.width / 3),
                    border: TableBorder.all(
                        color: Colors.black,
                        style: BorderStyle.solid,
                        width: 2),
                    children: [
                      TableRow(children: [
                        Column(children: [
                          Icon(
                            Icons.account_box,
                            size: iconSize,
                          ),
                          Text('My Account')
                        ]),
                        Column(children: [
                          Icon(
                            Icons.settings,
                            size: iconSize,
                          ),
                          Text('Settings')
                        ]),
                        Column(children: [
                          Icon(
                            Icons.lightbulb_outline,
                            size: iconSize,
                          ),
                          Text('Ideas')
                        ]),
                      ]),
                      TableRow(children: [
                        Icon(
                          Icons.cake,
                          size: iconSize,
                        ),
                        Icon(
                          Icons.voice_chat,
                          size: iconSize,
                        ),
                        Icon(
                          Icons.add_location,
                          size: iconSize,
                        ),
                      ]),
                    ],
                  ),
                ),
              ])),
              DataTable(
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      'Name',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Age',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'Role',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
                rows: const <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text('Sarah')),
                      DataCell(Text('19')),
                      DataCell(Text('Student')),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text('Janine')),
                      DataCell(Text('43')),
                      DataCell(Text('Professor')),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text('William')),
                      DataCell(Text('27')),
                      DataCell(Text('Associate Professor')),
                    ],
                  ),
                ],
              ),
              SizedBox(
                width: double.infinity,
                child: DataTable(
                  columns: const <DataColumn>[
                    DataColumn(
                      label: Text('Number'),
                    ),
                  ],
                  rows: List<DataRow>.generate(
                    numItems,
                    (index) => DataRow(
                      color: MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) {
                        // All rows will have the same selected color.
                        if (states.contains(MaterialState.selected))
                          return Theme.of(context)
                              .colorScheme
                              .primary
                              .withOpacity(0.08);
                        // Even rows will have a grey color.
                        if (index % 2 == 0) return Colors.grey.withOpacity(0.3);
                        return null; // Use default value for other states and odd rows.
                      }),
                      cells: [DataCell(Text('Row $index'))],
                      selected: selected[index],
                      onSelectChanged: (bool value) {
                        setState(() {
                          selected[index] = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ])
          ],
        ));
  }
}
