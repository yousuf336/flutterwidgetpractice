import 'package:flutter/material.dart';

class AnimatedContainerExample extends StatelessWidget {
  var _color = Colors.red;
  var _height = 100.0;
  var _width = 100.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animated Container"),
      ),
      body: MyStateFullWidget(),
    );
  }
}

class MyStateFullWidget extends StatefulWidget {
  @override
  _myStateFullWidgetState createState() => _myStateFullWidgetState();
}

class _myStateFullWidgetState extends State<MyStateFullWidget> {
  var _color = Colors.red;
  var _height = 100.0;
  var _width = 100.0;
  var _isFirstCrossFadeEnabled = false;
  var _opacity = 0.0;

  @override
  Widget build(BuildContext context) {
    animateContainer() {
      setState(() {
        _color = _color == Colors.red ? Colors.green : Colors.red;
        _height = _height == 100 ? 200 : 100;
        _width = _width == 100 ? 200 : 100;
      });
    }

    animateCrossFade() {
      setState(() {
        _isFirstCrossFadeEnabled = !_isFirstCrossFadeEnabled;
      });
    }

    animateOpacity() {
      setState(() {
        _opacity = _opacity == 0 ? 1.0 : 0;
      });
    }

    return ListView(
        children:<Widget>[
          Container(
      padding: EdgeInsets.all(20.0),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          AnimatedContainer(
            duration: Duration(seconds: 1),
            color: _color,
            width: _width,
            height: _height,
          ),
          OutlineButton(
            child: Text("Amnimate Container"),
            onPressed: () {
              animateContainer();
            },
          ),
          AnimatedCrossFade(
            duration: Duration(milliseconds: 3000),
            firstChild: Container(
              child: Image.asset('images/android.png'),
              height: 200,
              width: 200,
            ),
            secondChild: Container(
              child: Image(
                image: AssetImage('images/appale.png'),
              ),
            ),
            crossFadeState: _isFirstCrossFadeEnabled
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
          ),
          OutlineButton(
            child: Text("Animate CrossFade"),
            onPressed: () {
              animateCrossFade();
            },
          ),
          AnimatedOpacity(
            opacity: _opacity,
            duration: Duration(seconds: 2),
            child: FlutterLogo(
              size: 200.0,
            ),
          ),
          OutlineButton(
            child: Text("Animate Opacity"),
            onPressed: () {
              animateOpacity();
            },
          )
        ],
      ),
    )]);
  }
}
