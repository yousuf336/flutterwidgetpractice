import 'package:flutter/material.dart';

class ToolTip extends StatefulWidget {
  @override
  _ToolTipState createState() => _ToolTipState();
}

class _ToolTipState extends State<ToolTip> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Flutter Tooltip'),
          ),
          body: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                Container(
                  margin: EdgeInsets.all(10),
                  child: Tooltip(
                      message: 'My Account',
                      child: FlatButton(
                        onPressed: () {  },
                        child: Icon(
                          Icons.account_box,
                          size: 50,
                        ),
                      )),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: Tooltip(
                      message: 'Delete',
                      child: FlatButton(
                        onPressed: () {  },
                        child: Icon(
                          Icons.delete,
                          size: 50,
                        ),
                      )),
                ),
              ]))),
    );
  }
}
