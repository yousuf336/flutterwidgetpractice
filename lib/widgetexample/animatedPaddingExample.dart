import 'package:flutter/material.dart';

class AnimatedPaddingExample extends StatefulWidget {
  AnimatedPaddingExample({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AnimatedPaddingExampleState createState() => _AnimatedPaddingExampleState();
}

class _AnimatedPaddingExampleState extends State<AnimatedPaddingExample> {
  double inset = 5.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: AnimatedPadding(
            curve: Curves.slowMiddle,
            padding: EdgeInsets.all(inset),
            duration: Duration(milliseconds: 500),
            child: Image(
              image: AssetImage('assets/images/asparagus.jpg'),
            ),
          )),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FloatingActionButton(
              heroTag: 'increase',
              child: Icon(Icons.zoom_in),
              onPressed: () {
                setState(() {
                  inset += 10;
                });
              },
            ),
            FloatingActionButton(
              heroTag: 'decrease',
              child: Icon(Icons.zoom_out),
              onPressed: () {
                setState(() {
                  if (inset > 10) {
                    inset -= 10;
                  }
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
