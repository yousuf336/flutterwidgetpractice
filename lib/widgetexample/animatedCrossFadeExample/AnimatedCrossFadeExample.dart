import 'package:flutter/material.dart';

import 'login.dart';
import 'signup.dart';

class AnimatedCrossFadeExample extends StatefulWidget {
  AnimatedCrossFadeExample({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AnimatedCrossFadeExampleState createState() => _AnimatedCrossFadeExampleState();
}

class _AnimatedCrossFadeExampleState extends State<AnimatedCrossFadeExample> {
  bool flag = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            AnimatedCrossFade(
                firstChild: Login(),
                secondChild: SignUp(),
                crossFadeState:
                flag ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                duration: Duration(milliseconds: 500)),
            RaisedButton(
              child: Text('Done'),
              onPressed: () {},
            ),
            FlatButton(
              child: Text(flag ? 'Go to Sign Up' : 'Go to Login'),
              onPressed: () {
                setState(() {
                  flag = !flag;
                });
              },
            )
          ],
        ),
      ),
    );
  }
}