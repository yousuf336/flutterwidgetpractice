import 'package:flutter/material.dart';


class TwinAnimationBuilderExample extends StatefulWidget {
  @override
  _TwinAnimationBuilderExampleState createState() => _TwinAnimationBuilderExampleState();
}

class _TwinAnimationBuilderExampleState extends State<TwinAnimationBuilderExample> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  int i = 0;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );
    _controller.addListener(() {});
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void update() {
    setState(() {
      i = (_controller.value * 299792458).round();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TwinAnimationBuilder Example'),
      ),
      body: Center(
        child: TweenAnimationBuilder(
          tween: IntTween(begin: 0, end: 299792458),
          duration: const Duration(seconds: 5),
          builder: (BuildContext context, int i, Widget child) {
            return Text('$i m/s');
          },
        ),
      ),
      floatingActionButton:
      FloatingActionButton(child: Icon(Icons.add), onPressed: () {}),
    );
  }
}
