import 'package:flutter/material.dart';


class SelectableWidgetExample extends StatefulWidget {
  @override
  _SelectableWidgetExampleState createState() => _SelectableWidgetExampleState();
}

class _SelectableWidgetExampleState extends State<SelectableWidgetExample> {
  List<bool> isSelected;

  @override
  void initState() {
    super.initState();
    isSelected = [
      true,
      false,
      false,
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter 1.9 demo'),
      ),
      body: SingleChildScrollView(
        child: ColorFiltered(
          colorFilter: ColorFilter.mode(Colors.red, BlendMode.srcOut),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ToggleButtons(
                children: <Widget>[
                  Icon(Icons.home),
                  Icon(Icons.ac_unit),
                  Icon(Icons.access_alarm),
                ],
                isSelected: isSelected,
                onPressed: (index) {
                  setState(() {
                    for (var i = 0; i < isSelected.length; i++) {
                      if (i == index) {
                        isSelected[i] = true;
                      } else {
                        isSelected[i] = false;
                      }
                    }
                  });
                },
              ),
              const SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: SelectableText(
                  "This is a selectable text, try selecting by long tap.",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}