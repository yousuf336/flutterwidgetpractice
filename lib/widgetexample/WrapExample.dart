import 'package:flutter/material.dart';

class WrapExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget MyButton(var text) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 1),
        child: RaisedButton.icon(
          textColor: Colors.white,
          color: Color(0xFF6200EE),
          onPressed: () {
            // Respond to button press
          },
          icon: Icon(Icons.add, size: 18),
          label: Text(text),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Wrap Example",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
        ),
      ),
      body: Center(
          child: Column(children: [
        Spacer(),
        Center(
            child: Text(
          "Without Wrap ..",
          style: TextStyle(fontSize: 25, color: Colors.black),
        )),
        Row(
          children: [
            MyButton("Button 1"),
            MyButton("Button 2"),
            MyButton("Button 3"),
            MyButton("Button 4"),
            MyButton("Button 5"),
          ],
        ),
        Spacer(),
        Center(
            child: Text(
          "With Wrap ..",
          style: TextStyle(fontSize: 25, color: Colors.black),
        )),
        Wrap(
          alignment: WrapAlignment.spaceBetween,
          runAlignment: WrapAlignment.end,//vertical
          direction: Axis.horizontal,
          spacing: 10.0,
          runSpacing: 10.0,//vertical
          verticalDirection: VerticalDirection.up,
          children: [
            MyButton("Button 1"),
            MyButton("Button 2"),
            MyButton("Button 3"),
            MyButton("Button 4"),
            MyButton("Button 5"),
            MyButton("Button 6"),
            MyButton("Button 7"),
            MyButton("Button 8"),
            MyButton("Button 9"),
            MyButton("Button 10"),
            MyButton("Button11"),
            MyButton("Button 12"),
            MyButton("Button 13"),
            MyButton("Button 14"),
          ],
        ),
        Spacer(),
      ])),
    );
  }
}
