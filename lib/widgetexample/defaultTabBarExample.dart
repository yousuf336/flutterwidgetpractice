
import 'package:flutter/material.dart';

class TabPage extends StatelessWidget {
  var tabIndex = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: tabIndex,
      child: Scaffold(
        appBar: AppBar(),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            Container(
              color: Colors.green,
              child: Center(
                child: GoToThirdTabButton(),
              ),
            ),
            Container(color: Colors.red),
            Container(color: Colors.yellow),
            Container(color: Colors.cyan),
          ],
        ),
        bottomNavigationBar: TabBar(
          labelColor: Colors.black45,
          tabs: [
            Padding(padding: const EdgeInsets.only(top: 12, bottom: 12), child: Text('green')),
            Padding(padding: const EdgeInsets.only(top: 12, bottom: 12), child: Text('red')),
            Padding(padding: const EdgeInsets.only(top: 12, bottom: 12), child: Text('yellow')),
            Padding(padding: const EdgeInsets.only(top: 12, bottom: 12), child: Text('cyan')),
          ],
        ),
      ),
    );
  }
}

class GoToThirdTabButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        child: Text('to Tab 3'),
        onPressed: () {
          DefaultTabController.of(context).animateTo(2);
        }
    );
  }
}