import 'package:flutter/material.dart';

class AspectRationExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AspectRatio Example'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.green,
        child: Center(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.green,
            alignment: Alignment.center,
            child: AspectRatio(
              aspectRatio: 1 / 8,
              child: Container(
                color: Colors.orangeAccent,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
