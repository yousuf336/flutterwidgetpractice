import 'package:flutter/material.dart';

class ClipExample extends StatefulWidget {
  @override
  _ClipExampleState createState() => _ClipExampleState();
}

class _ClipExampleState extends State<ClipExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          title: Text(
            "Clip Example",
            style: TextStyle(fontSize: 20),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 20),
            alignment: Alignment.topCenter,
            child: Column(children: <Widget>[
              ClipOval(
                child: Padding(
                  padding: EdgeInsets.all(0),
                  child: Image(
                    image: AssetImage('images/appale.png'),
                    height: 200,
                    width: 200,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(150.0),
                    topRight: Radius.circular(55.0),
                    bottomLeft: Radius.circular(5.0),
                    bottomRight: Radius.circular(250.0)),
                child: Image(
                  image: AssetImage('images/appale.png'),
                  height: 200,
                  width: 200,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ClipRect(
                //  clipper: TriangleClip(),
                child: Image(
                  image: AssetImage('images/appale.png'),
                  height: 200,
                  width: 200,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ClipPath(
                  clipper: TriangleClip(),
                child: Image(
                  image: AssetImage('images/appale.png'),
                  height: 200,
                  width: 200,
                  fit: BoxFit.cover,
                ),
              ),
            ]),
          ),
        ));
  }
}

class TriangleClip extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width, 0.0);
    path.lineTo(size.width / 2, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
