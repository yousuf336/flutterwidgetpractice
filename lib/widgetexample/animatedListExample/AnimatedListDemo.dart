import 'package:flutter/material.dart';
import 'package:flutter_widget_example/widgetexample/animatedListExample/widget/shopping_item_widget.dart';

import 'data.dart';
import 'model/shopping_item.dart';


class AnimatedListDemo extends StatefulWidget {
  final String title;

  const AnimatedListDemo({
    @required this.title,
  });

  @override
  _AnimatedListDemoState createState() => _AnimatedListDemoState();
}

class _AnimatedListDemoState extends State<AnimatedListDemo> {
  final key = GlobalKey<AnimatedListState>();
  final items = List.from(Data.shoppingList);

  @override
  Widget build(BuildContext context) => Scaffold(
    backgroundColor: Colors.deepPurple,
    appBar: AppBar(
      title: Text(widget.title),
    ),
    body: Column(
      children: [
        Expanded(
          child: AnimatedList(
            key: key,
            initialItemCount: items.length,
            itemBuilder: (context, index, animation) =>
                buildItem(items[index], index, animation),
          ),
        ),
        Container(
          padding: EdgeInsets.all(16),
          child: buildInsertButton(),
        ),
      ],
    ),
  );

  Widget buildItem(item, int index, Animation<double> animation) =>
      ShoppingItemWidget(
        item: item,
        animation: animation,
        onClicked: () => removeItem(index),
      );

  Widget buildInsertButton() => RaisedButton(
    child: Text(
      'Insert item',
      style: TextStyle(fontSize: 20),
    ),
    color: Colors.white,
    onPressed: () => insertItem(3, Data.shoppingList.first),
  );

  void insertItem(int index, ShoppingItem item) {
    items.insert(index, item);
    key.currentState.insertItem(index);
  }

  void removeItem(int index) {
    final item = items.removeAt(index);

    key.currentState.removeItem(
      index,
          (context, animation) => buildItem(item, index, animation),
    );
  }
}