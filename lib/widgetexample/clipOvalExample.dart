
import 'package:flutter/material.dart';


class ClipOvalExample extends StatefulWidget {
  @override
  _ClipOvalExampleState createState() => _ClipOvalExampleState();
}

class _ClipOvalExampleState extends State<ClipOvalExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ClipOval'),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: ClipOval(
          child: Image.network(
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRFU7U2h0umyF0P6E_yhTX45sGgPEQAbGaJ4g&usqp=CAU',
              height: 200,
              width: 200,
              fit: BoxFit.fill),
          clipper: MyClip(),
        ),
      ),
      backgroundColor: Colors.lightBlue[50],
    );
  }
}

class MyClip extends CustomClipper<Rect> {
  Rect getClip(Size size) {
    return Rect.fromLTWH(0, 0, 200, 200);
  }

  bool shouldReclip(oldClipper) {
    return true;
  }
}