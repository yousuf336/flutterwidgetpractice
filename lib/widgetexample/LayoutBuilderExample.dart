import 'package:flutter/material.dart';

class LayoutBuilderExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('LayoutBuilder Example')),
        body: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth > 600.0) {
            return _myTimeLisePhoneView();
          } else {
            return  _myTimeLiseTabView();
           // return _myTimeLisePhoneView();
          }
        }));
  }

  Widget _myTimeLisePhoneView() {
    return SingleChildScrollView(
      child: Column(children: [
        Container(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 54.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 30.0,
                      height: 30.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 10.0, color: Colors.green),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Start',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 54.0),
                child: Row(
                  children: [
                    Container(
                      width: 5.0,
                      height: 80.0,
                      color: Colors.green,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 44.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 54.0),
                child: Row(
                  children: [
                    Container(
                      width: 5.0,
                      height: 80.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 44.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 54.0),
                child: Row(
                  children: [
                    Container(
                      width: 5.0,
                      height: 80.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 44.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 54.0),
                child: Row(
                  children: [
                    Container(
                      width: 5.0,
                      height: 80.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 44.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 54.0),
                child: Row(
                  children: [
                    Container(
                      width: 5.0,
                      height: 80.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }

  Widget _myTimeLiseTabView() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Column(children: [
        Container(
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 34.0, top: 8.0),
                child: Column(
                  children: [
                    Container(
                      width: 30.0,
                      height: 30.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 10.0, color: Colors.green),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Start',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Row(
                  children: [
                    Container(
                      width: 80.0,
                      height: 5.0,
                      color: Colors.green,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 4.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Row(
                  children: [
                    Container(
                      width: 80.0,
                      height: 5.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 4.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Row(
                  children: [
                    Container(
                      width: 80.0,
                      height: 5.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 4.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Row(
                  children: [
                    Container(
                      width: 80.0,
                      height: 5.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 4.0, top: 8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      padding: const EdgeInsets.all(1.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(width: 30.0, color: Colors.red),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        child: Text(
                          'Second',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Row(
                  children: [
                    Container(
                      width: 80.0,
                      height: 5.0,
                      color: Colors.red,
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
