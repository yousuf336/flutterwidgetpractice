import 'package:flutter/material.dart';

class DraggableScrollableSheetExample extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Flutter Demo'),
        ),
        body: Material(
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 0,
                bottom: 150,
                left: 0,
                right: 0,
                child: Container(
                  color: Color.fromARGB(100, 100, 100, 100),
                  child: Image.network(
                    'https://i.pinimg.com/originals/ca/76/0b/ca760b70976b52578da88e06973af542.jpg',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              DraggableScrollableSheet(
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return Container(
                    color: Colors.white,
                    child: ListView.builder(
                        controller: scrollController,
                        itemCount: 20,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            padding: EdgeInsets.all(5),
                            child: Card(
                                child: ListTile(
                              title: Text('Item $index'),
                            )),
                          );
                        }),
                  );
                },
              )
            ],
          ),
        ));
  }
}
