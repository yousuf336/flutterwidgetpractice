import 'package:flutter/material.dart';

class PlaceHolderExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PlaceHolder Example'),
      ),
      body:  Column(
        children: <Widget>[
          Container(
              child: Placeholder()
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Placeholder(),
                ),
                Flexible(
                  flex: 1,
                  child: Placeholder(
                  ),
                ),
              ],
            ),
          )
        ],
      )
    );
  }
}
