import 'package:flutter/material.dart';

class OpacityExample extends StatelessWidget {
  Widget MyWidget(Color color) {
    return Container(
      height: 80,
      width: 50,
      color: color,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 120),
    );
  }

  Widget MyGradientWidget() {
    return Container(
      height: 80,
      width: 50,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 120),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end:
              Alignment(0.8, 0.0), // 10% of the width, so there are ten blinds.
          colors: [
            const Color(0xffee0000),
            const Color(0xffeeee00)
          ], // red to yellow
          tileMode: TileMode.repeated, // repeats the gradient over the canvas
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Opacity Example"),
      ),
      body: ListView(
        children: <Widget>[
          Spacer(),
          Center(
              child: Text(
            "Without Opacity",
            style: TextStyle(fontSize: 25),
          )),
          MyWidget(Colors.red),
          MyGradientWidget(),
          MyWidget(Colors.green),
          Spacer(),
          Center(
              child: Text(
            "With Opacity",
            style: TextStyle(fontSize: 25),
          )),
          MyWidget(Colors.red),
          Opacity(
            opacity: 0.15,
            child: MyGradientWidget(),
          ),
          MyWidget(Colors.green),
          Spacer(),
        ],
      ),
    );
  }
}
