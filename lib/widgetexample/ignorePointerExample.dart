import 'package:flutter/material.dart';

class IgnorePointerExample extends StatefulWidget {
  @override
  _AbsorbPointerAppState  createState() => _AbsorbPointerAppState ();
}

class _AbsorbPointerAppState extends State<IgnorePointerExample> {
  bool _absorbing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('IgnorPointer'),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AbsorbPointer(
                  absorbing: _absorbing,
                  child: Column(
                    children: <Widget>[
                      RaisedButton(
                        child: Text('Press the button'),
                        onPressed: () {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('Button is pressed'))
                          );
                        },
                      ),
                      TextField(),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Absorb Pointer?'),
                    Switch(
                        value: _absorbing,
                        onChanged: (bool value) {
                          setState(() {
                            _absorbing = value;
                          });
                        }),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
