import 'package:flutter/material.dart';

class FadeInImageExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FadeImage Example"),
      ),
      body: Stack(children: [
        Center(
          child: CircularProgressIndicator(),
        ),
        Center(
          child: FadeInImage.assetNetwork(
          //  placeholder: 'images/loading.gif',
            placeholder: 'images/loading.gif',
            image: 'https://thumbs.dreamstime.com/z/panoramic-landscape-multicolor-spring-flowers-nature-backg-park-background-91174058.jpg',
          ),
        ),
      ]),
    );
  }
}
