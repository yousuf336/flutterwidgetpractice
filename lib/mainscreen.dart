import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_widget_example/widgetexample/AlignExample.dart';
import 'package:flutter_widget_example/widgetexample/AnimatedBuilderExample.dart';
import 'package:flutter_widget_example/widgetexample/AnimatedSwitcherExample.dart';
import 'package:flutter_widget_example/widgetexample/BackdropFilterExample.dart';
import 'package:flutter_widget_example/widgetexample/FlexibleWidgetExample.dart';
import 'package:flutter_widget_example/widgetexample/IndexedStackExample.dart';
import 'package:flutter_widget_example/widgetexample/InheritedWidgetExample/InheritedWidgetExample.dart';
import 'package:flutter_widget_example/widgetexample/LayoutBuilderExample.dart';
import 'package:flutter_widget_example/widgetexample/LimitedBoxExample.dart';
import 'package:flutter_widget_example/widgetexample/RichTextWidget.dart';
import 'package:flutter_widget_example/widgetexample/SelectableWidgetExample.dart';
import 'package:flutter_widget_example/widgetexample/aboutDialogExample.dart';
import 'package:flutter_widget_example/widgetexample/animatedCrossFadeExample/AnimatedCrossFadeExample.dart';
import 'package:flutter_widget_example/widgetexample/animatedIconExample.dart';
import 'package:flutter_widget_example/widgetexample/animatedListExample.dart';
import 'package:flutter_widget_example/widgetexample/animatedListExample/AnimatedListDemo.dart';
import 'package:flutter_widget_example/widgetexample/animatedOpacityExample.dart';
import 'package:flutter_widget_example/widgetexample/animatedPaddingExample.dart';
import 'package:flutter_widget_example/widgetexample/animatedPositionedExample/AnimatedPositionedExample.dart';
import 'package:flutter_widget_example/widgetexample/aspectRationExample.dart';
import 'package:flutter_widget_example/widgetexample/carouselExample/carouselExample.dart';
import 'package:flutter_widget_example/widgetexample/checkboxListTileExample.dart';
import 'package:flutter_widget_example/widgetexample/circularProgressBarExample.dart';
import 'package:flutter_widget_example/widgetexample/clipOvalExample.dart';
import 'package:flutter_widget_example/widgetexample/clipPathExample.dart';
import 'package:flutter_widget_example/widgetexample/cupertionActionSheetExample.dart';
import 'package:flutter_widget_example/widgetexample/defaultTabBarExample.dart';
import 'package:flutter_widget_example/widgetexample/dialogExample/dialog/ConfirmationDialog.dart';
import 'package:flutter_widget_example/widgetexample/dismissibleExample/dismissibleExample.dart';
import 'package:flutter_widget_example/widgetexample/dividerExample.dart';
import 'package:flutter_widget_example/widgetexample/dragableExample.dart';
import 'package:flutter_widget_example/widgetexample/draggablescrollablesheetExample.dart';
import 'package:flutter_widget_example/widgetexample/drawerExample.dart';
import 'package:flutter_widget_example/widgetexample/fitBoxExample.dart';
import 'package:flutter_widget_example/widgetexample/fractionallySizedBoxExample.dart';
import 'package:flutter_widget_example/widgetexample/ignorePointerExample.dart';
import 'package:flutter_widget_example/widgetexample/imageFilteres/imageFiltereExample.dart';
import 'package:flutter_widget_example/widgetexample/listWhileScrollviewExample.dart';
import 'package:flutter_widget_example/widgetexample/notificationListenerExample.dart';
import 'package:flutter_widget_example/widgetexample/placeHolderExample.dart';
import 'package:flutter_widget_example/widgetexample/reorderAbleListView.dart';
import 'package:flutter_widget_example/widgetexample/shaderMaskExample.dart';
import 'package:flutter_widget_example/widgetexample/sliderExample/SliderExample.dart';
import 'package:flutter_widget_example/widgetexample/snackbarExample.dart';
import 'package:flutter_widget_example/widgetexample/toggleButton.dart';
import 'package:flutter_widget_example/widgetexample/transform_demo.dart';
import 'package:flutter_widget_example/widgetexample/twinAnimationBuilderExample.dart';
import 'package:flutter_widget_example/widgetexample/valueLIstenableBulder.dart';
import 'widgetexample/safeArea.dart';
import 'widgetexample/ExpandedExample.dart';
import 'widgetexample/WrapExample.dart';
import 'widgetexample/AnimatedContainerExample.dart';
import 'widgetexample/OpacityExample.dart';
import 'widgetexample/FutureBuilderExample.dart';
import 'widgetexample/FadeExample.dart';
import 'widgetexample/PageViewExample.dart';
import 'widgetexample/TableExample.dart';
import 'widgetexample/SliverExample/SliverAppBarExample.dart';
import 'widgetexample/FadeInImageExample.dart';
import 'widgetexample/StreamBuilderExample.dart';
import 'widgetexample/ClipExample.dart';
import 'widgetexample/heroExample/HeroExample.dart';
import 'widgetexample/paintExample.dart';
import 'widgetexample/toolTip.dart';
import 'widgetexample/stackeditem/StackedItemExample.dart';

class MainScreen extends StatelessWidget {
  Widget _MyButton(
          BuildContext context, String text, dynamic onClickListener) =>
      Center(
        child: Container(
          width: 190,
          child: RaisedButton(
            child: Container(
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: Text(
                text,
                style: TextStyle(
                  color: Colors.white,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            color: Colors.blue,
            onPressed: () {
              onClickListener();
            },
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(icon: Icon(Icons.menu), onPressed: () {}),
          title: Text("MainScreen"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _MyButton(context, "SafeArea", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SafeAreaExample()));
              }),
              _MyButton(context, "Expanded", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ExpandedExample()));
              }),
              _MyButton(context, "Wrap Example", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => WrapExample()));
              }),
              _MyButton(context, "Animated Container", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedContainerExample()));
              }),
              _MyButton(context, "Opacity Example", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OpacityExample()));
              }),
              _MyButton(context, "Future Builder", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FutureBuilderExample()));
              }),
              _MyButton(context, "Fade Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            FadeExample(title: "Flutter Example")));
              }),
              _MyButton(context, "PageView Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        fullscreenDialog: true,
                        builder: (context) => PageViewExample()));
              }),
              _MyButton(context, "Table Example", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TableExample()));
              }),
              _MyButton(context, "Sliver Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SliverAppBarExample()));
              }),
              _MyButton(context, "FadeImage Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FadeInImageExample()));
              }),
              _MyButton(context, "StreamBuilder Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => StreamBuilderExample()));
              }),
              _MyButton(context, "InheritedWidget Exm", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InheritedWidgetExample()));
              }),
              _MyButton(context, "Clip Exmample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ClipExample()));
              }),
              _MyButton(context, "Hero Exmample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HeroExample()));
              }),
              _MyButton(context, "Paint Exmample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaintExample()));
              }),
              _MyButton(context, "ToolTip Exmample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ToolTip()));
              }),
              _MyButton(context, "FitBox Exmample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FitBoxExample()));
              }),
              _MyButton(context, "LayoutBuilder Exmample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LayoutBuilderExample()));
              }),
              _MyButton(context, "TransForm Exmample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TransformDemo()));
              }),
              _MyButton(context, "BackdropFilter Exmample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => BackdropFilterExample()));
              }),
              _MyButton(context, "Align Exmample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AlignExample()));
              }),
              _MyButton(context, "StackItem Exmample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => StackItemExample()));
              }),
              _MyButton(context, "AnimatedBuilder Exmample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedBuilderExample()));
              }),
              _MyButton(context, "Dismissible Exmample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DismissibleExample(title: 'Dismissible Example')));
              }),
              _MyButton(context, "ValueListenableBuilder", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ValueListenableBuilderExample(
                            title: 'ValueListenableBuilder')));
              }),
              _MyButton(context, "DragableWidgetExample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DragableWidgetExample()));
              }),
              _MyButton(context, "AnimatedList", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedListExample()));
              }),
              _MyButton(context, "Flexible Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FlexiableWidgetExample()));
              }),
              _MyButton(context, "AnimatedIcon", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedIconExample()));
              }),
              _MyButton(context, "AspectRation Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AspectRationExample()));
              }),
              _MyButton(context, "LimitedBox Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LimitedBoxWidget()));
              }),
              _MyButton(context, "PlaceHolder Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PlaceHolderExample()));
              }),
              _MyButton(context, "RichText Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            RichTextExample(title: 'RichText Example')));
              }),
              _MyButton(context, "ReOrderListView ", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ReorderAbleListViewExample()));
              }),
              _MyButton(context, "AnimatedSwitcher ", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedSwitcherExample()));
              }),
              _MyButton(context, "AnimatedPosition ", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedPositionedDemo()));
              }),
              _MyButton(context, "AnimatedPadding", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AnimatedPaddingExample(title: 'AnimatedPadding')));
              }),
              _MyButton(context, "IndexedStackExample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => IndexedStackExample()));
              }),
              _MyButton(context, "AnimatedOpacity Demo", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedOpacityRecipe(
                            title: 'AnimatedOpacity Demo')));
              }),
              _MyButton(context, "FractionallySizedBox  Demo", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FractionallySizedBoxExample()));
              }),
              _MyButton(context, "AnimatedListExample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedListDemo(
                              title: 'AnimatedList Demo',
                            )));
              }),
              _MyButton(context, "SelectAbleWidgetExample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SelectableWidgetExample()));
              }),
              _MyButton(context, "SliderExample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SliderExample()));
              }),
              _MyButton(context, "AlertDialog", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ConfirmationDialog()));
              }),
              _MyButton(context, "AnimatedCrossFadeExample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AnimatedCrossFadeExample(
                              title: 'AnimatedCrossFade Example',
                            )));
              }),
              _MyButton(context, "DragableScrollableSheet", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DraggableScrollableSheetExample()));
              }),
              _MyButton(context, "ImageFilters Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ImageFilteresExample()));
              }),
              _MyButton(context, "ToggleButton Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ToggleButtonExample()));
              }),
              _MyButton(context, "CupertinoActionSheet Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CuperTinoActionSheetExample()));
              }),
              _MyButton(context, "TwinAnimationBuilder", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TwinAnimationBuilderExample()));
              }),
              _MyButton(context, "Default TabExample", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TabPage()));
              }),
              _MyButton(context, "Drawer Example", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DrawerExample()));
              }),
              _MyButton(context, "Drawer Example", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DrawerExample()));
              }),
              _MyButton(context, "SnckBar Example", () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SnackBarExample()));
              }),
              _MyButton(context, "ListWheelScrollView", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListWheelScrollViewApp()));
              }),
              _MyButton(context, "ShaderMask Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ShaderMaskExample()));
              }),
              _MyButton(context, "NotificationListener", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => NotificationListenerExample()));
              }),
              _MyButton(context, "ClipPathExample", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ClipPathExample()));
              }),
              _MyButton(context, "CircularProgressBar", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CircularProgressIndicatorExample()));
              }),

              _MyButton(context, "Divider Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DividerExample()));
              }),
              _MyButton(context, "IgnorePointer Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => IgnorePointerExample()));
              }),
              _MyButton(context, "ClipOval Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ClipOvalExample()));
              }),
              _MyButton(context, "CheckboxListTile Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxListTileExample()));
              }),
              _MyButton(context, "AboutDialog Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AboutDialogExample()));
              }),
              _MyButton(context, "Carousel Example", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CarouselDemo()));
              }),
            ],
          ),
        ));
  }
}
